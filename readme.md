# WP Plugin Boilerplate
## Get started
Pour commencer, il faut remplacer les différents noms utilisés par le nom de votre plugin.

Remplacer `wp-plugin-boilerplate` par `plugin-name`
Remplacer `WP_Plugin_Boilerplate` par `Plugin_Name`
Remplacer `WPPB` par `PN` or Constant prefix.
Remplacer `WP Plugin Boilerplate` par `Plugin Name`
Remplacer `whoz_core` par `plugin_name`

Lancez les commandes `npm install` et `composer install` pour installer les dépendances.

## Informations
Ce boilerplate contient une classe permettant de gérer les icônes : WP_Plugin_Boilerplate_Icons, située dans le dossier includes.
Il est également configuré pour utiliser gulp, ainsi que composer.

⚠ Le dossier vendor est inclus dans le `.gitignore` : pensez à le modifier au besoin.

## Gulp : compilation et configuration.
... A venir.
